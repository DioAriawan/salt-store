import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native'
import { TextInput } from 'react-native-gesture-handler'
import { Value } from 'react-native-reanimated'
import Icon from 'react-native-vector-icons/AntDesign'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

const Tab = createBottomTabNavigator();
export default class index extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={{backgroundColor:'#48cae4',height:75,width:'100%',justifyContent:'center'}}>
                    <Text style={{fontSize:20,fontWeight:'bold',paddingHorizontal:10}}>Home</Text>
                </View>
                <View style={styles.textphoto}>
                    <Image style={styles.profile} source={{uri:'https://akcdn.detik.net.id/community/media/visual/2019/02/19/42393387-9c5c-4be4-97b8-49260708719e.jpeg?w=750&q=90'}}/>
                    <Text style={{fontSize:30,fontWeight:'bold'}}>Jack</Text>
                </View>
                <View style={{alignItems:'center'}}>
                    <View style={styles.input}>
                        <Icon name='search1' size={25} color='black'/>
                        <TextInput placeholder='Search'/>
                    </View>
                </View>
                <Text style={{fontSize:30,marginLeft:15,fontWeight:'bold'}}>Catagories</Text>
                <View style={styles.box}>
                    <TouchableOpacity>
                        <View style={styles.salt}>
                            <Text style={{fontSize:23,fontWeight:'bold'}}>Sea Salt</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.salt}>
                            <Text style={{fontSize:18,fontWeight:'bold'}}>Kosher Salt</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.salt}>
                            <Text style={{fontSize:21,fontWeight:'bold'}}>Celtic Salt</Text>
                        </View>
                    </TouchableOpacity>  
                </View>
                <ScrollView style={{marginTop:10}}>
                    <View style={{alignItems:'center'}}>    
                        <View style={styles.list}>
                            <View style={styles.saltList}>
                                <Image style={{height:75,width:75,borderRadius:10,marginTop:5}} source={{uri:'https://static.vecteezy.com/system/resources/thumbnails/004/458/576/small/sea-salt-flat-linear-long-shadow-icon-spa-salon-sea-salt-bottle-line-symbol-vector.jpg'}}/>
                                <View style={styles.textSalt}>
                                    <Text style={styles.textSalt}>Sea Salt</Text>
                                    <Text style={styles.textSalt}>Rp.100.000</Text>
                                    <Text style={styles.textSalt}>Rating:8,7</Text>
                                </View>
                                <View style={{backgroundColor:'#48cae4',width:60,height:30,justifyContent:'flex-end',marginTop:30,borderRadius:10,justifyContent:'center',alignItems:'center'}}>
                                    <Text style={{fontWeight:'bold',fontSize:18}}>Buy</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.list}>
                            <View style={styles.saltList}>
                                <Image style={{height:75,width:75,borderRadius:10,marginTop:5}} source={{uri:'https://static.vecteezy.com/system/resources/thumbnails/004/458/576/small/sea-salt-flat-linear-long-shadow-icon-spa-salon-sea-salt-bottle-line-symbol-vector.jpg'}}/>
                                <View style={styles.textSalt}>
                                    <Text style={styles.textSalt}>Sea Salt</Text>
                                    <Text style={styles.textSalt}>Rp.100.000</Text>
                                    <Text style={styles.textSalt}>Rating:8,7</Text>
                                </View>
                                <View style={{backgroundColor:'#48cae4',width:60,height:30,justifyContent:'flex-end',marginTop:30,borderRadius:10,justifyContent:'center',alignItems:'center'}}>
                                    <Text style={{fontWeight:'bold',fontSize:18}}>Buy</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.list}>
                            <View style={styles.saltList}>
                                <Image style={{height:75,width:75,borderRadius:10,marginTop:5}} source={{uri:'https://static.vecteezy.com/system/resources/thumbnails/004/458/576/small/sea-salt-flat-linear-long-shadow-icon-spa-salon-sea-salt-bottle-line-symbol-vector.jpg'}}/>
                                <View style={styles.textSalt}>
                                    <Text style={styles.textSalt}>Sea Salt</Text>
                                    <Text style={styles.textSalt}>Rp.100.000</Text>
                                    <Text style={styles.textSalt}>Rating:8,7</Text>
                                </View>
                                <View style={{backgroundColor:'#48cae4',width:60,height:30,justifyContent:'flex-end',marginTop:30,borderRadius:10,justifyContent:'center',alignItems:'center'}}>
                                    <Text style={{fontWeight:'bold',fontSize:18}}>Buy</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#ADD8E6',
        
    },
    profile:{
        height:70,
        width:70,
        borderRadius:50,
        margin:10,
    },
    text:{
        fontSize:30,
        marginVertical:15,
        flexDirection:'row',
    },
    input:{
        borderWidth:1,
        width:350,
        flexDirection:'row',
        alignItems:'center',
        borderRadius:10,
    },
    textphoto:{
        flexDirection:'row',
        alignItems:'center'
    },
    box:{
        flexDirection:'row',
        justifyContent:'center',
        marginTop:10
    },
    salt:{
        backgroundColor:'#48cae4',
        height:50,
        width:100,
        marginHorizontal:15,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:17
    },
    list:{
        backgroundColor:'#caf0f8',
        height:100,
        width:350,
        borderRadius:10, 
        justifyContent:'center',
        marginTop:20
    },
    saltList:{
        flexDirection:'row',
        justifyContent:'space-evenly'
    },
    textSalt:{
        fontWeight:'bold',
        fontSize:20
    },
})
