import React, { Component } from 'react'
import { Text, StyleSheet, View , Image} from 'react-native'
import { TextInput } from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/AntDesign'
import { ID } from '../../asset/background'

export default class index extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={{backgroundColor:'#48cae4',height:75,width:'100%',justifyContent:'center'}}>
                    <Text style={{fontSize:20,fontWeight:'bold',paddingHorizontal:10}}>Profile</Text>
                </View>
                    <Image style={styles.profile} source={{uri:'https://akcdn.detik.net.id/community/media/visual/2019/02/19/42393387-9c5c-4be4-97b8-49260708719e.jpeg?w=750&q=90'}}/>
                    <Text style={styles.text}>Jack Sully</Text>
                    <View style={styles.block}>
                        <View style={{backgroundColor:'#ade8f4',height:50,width:300,alignSelf:'center',marginTop:30,alignItems:'center',justifyContent:'center',flexDirection:'row'}}>
                            <Icon name='solution1' size={30} color='black'/>
                            <Text style={styles.textPro}>Jack</Text>
                        </View>
                        <View style={{backgroundColor:'#ade8f4',height:50,width:300,alignSelf:'center',marginTop:30,alignItems:'center',justifyContent:'center',flexDirection:'row'}}>
                            <Icon name='mail' size={30} color='black'/>
                            <Text style={styles.textPro}>Jacky@gmail.com</Text>
                        </View>
                        <View style={{backgroundColor:'#ade8f4',height:50,width:300,alignSelf:'center',marginTop:30,alignItems:'center',justifyContent:'center',flexDirection:'row'}}>
                            <Icon name='phone' size={30} color='black'/>
                            <Text style={styles.textPro}>+69 098 321</Text>
                        </View>
                        <View style={{backgroundColor:'#ade8f4',height:50,width:300,alignSelf:'center',marginTop:30,alignItems:'center',justifyContent:'center',flexDirection:'row'}}>
                            <Icon name='enviromento' size={30} color='black'/>
                            <Text style={styles.textPro}>New York City</Text>
                        </View>
                        
                        
                    </View>

            </View>
        )
    }
}
                    

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#ADD8E6',
        alignItems:'center',
    },
    profile:{
        height:100,
        width:100,
        borderRadius:50,
        marginTop:30
    },
    text:{
        fontSize:30
    },
    box:{
        backgroundColor:'#00FFFF',
        height:100,
        width:300
    },
    block:{
        backgroundColor:'#caf0f8',
        height:400,
        width:'100%',
        margin:40,
        borderRadius:30,
    },
    textPro:{
        fontSize:20,
        fontWeight:'bold',
        
    }
})
