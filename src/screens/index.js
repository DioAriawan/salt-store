import Home from './home'
import Profile from './profile'
import Splash from './splash'

export {Home, Profile, Splash}