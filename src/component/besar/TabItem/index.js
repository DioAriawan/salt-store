import React from 'react'
import { StyleSheet, Text ,TouchableOpacity} from 'react-native'
// import { IconHome,IconProfile,IconActive } from '../../../asset/Icon/index' masalah di di sini
import Icon from 'react-native-vector-icons/AntDesign'


const TabItem = ({isFocused ,onLongPress , onPress ,label}) => {
    // const Icon = () => {
    //     if(label === 'Home') {
    //         return isFocused ? <IconHome/> : <IconActive/>
    //     }
    //     if(label === 'Profile') {
    //         return isFocused ? <IconProfile/> : <IconActive/>
    //     }
    //      return <IconProfile/>
    //     }
     return (
            <TouchableOpacity 
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{ flex: 1, }}>
            <Icon name='search1' size={30}/>        
            {/* jadi nya saya pake icon biasa yang slicing nya default */}
            <Text style={{flexDirection:'row'}}>
              {label}
            </Text>
          </TouchableOpacity>
    )
}

export default TabItem

const styles = StyleSheet.create({})
