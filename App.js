import React, { Component } from 'react'
import { Text, View } from 'react-native'
import Router from './src/navigation'

export class App extends Component {
    render() {
        return (
        <Router/>
        )
    }
}

export default App
